import React from 'react';
import './App.css';
import { Route, Switch, Router } from 'react-router-dom';
import HomeComponent from './containers/home/home';
import AddDetailsComponent from './containers/add-details/add-details';
import LoginComponent from './containers/login/login';
import HeaderComponent from './components/layout/header/header';
import history from './history';

function App() {

  const DefaultContainer = () => (
    <div>
      <HeaderComponent />
      <Route path="/home" exact component={HomeComponent} />
      <Route path="/login" exact component={LoginComponent} />
      <Route path="/addDetails" exact component={AddDetailsComponent} />
    </div>
  )

  return (
    <div className="App">
      <Router history={history}>
        <Switch>
          <Route path="/" exact component={LoginComponent} />
          <Route component={DefaultContainer} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
