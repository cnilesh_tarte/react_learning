import {combineReducers} from 'redux';
import authReducer from './auth';
import listPostReducer from './post/list-post-reducer';
import deletePostReducer from './post/delete-post-reducer';
import addPostReducer from './post/add-post-reducer';

export default combineReducers({
    authData : authReducer,
    listPosts : listPostReducer,
    deletePost : deletePostReducer,
    addPost : addPostReducer
});