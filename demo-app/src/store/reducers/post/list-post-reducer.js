import * as actionTypes from '../../actions/actionTypes';
import { updateObject } from '../../../shared/utility';


const initialState = {
    posts : []
};

const listPostsStart = ( state, action ) => {
    return updateObject( state, { error: null, loading: true } );
};

const listPostsSuccess = (state, action) => {
    return updateObject( state, { 
        posts: action.posts,
     } );
};

const listPostsFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        loading: false
    });
};


const listPostReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.LIST_POST_START : return listPostsStart(state, action);
        case actionTypes.LIST_POST_SUCCESS : return listPostsSuccess(state, action);
        case actionTypes.LIST_POST_FAIL : return listPostsFail(state, action);
        default:
            return state;
    }
};

export default listPostReducer;