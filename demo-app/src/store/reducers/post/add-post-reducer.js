import * as actionTypes from '../../actions/actionTypes';
import { updateObject } from '../../../shared/utility';


const initialState = {
    posts : {}
};

const addPostsStart = ( state, action ) => {
    return updateObject( state, { error: null, loading: true } );
};

const addPostsSuccess = (state, action) => {
    return updateObject( state, { 
        posts: action.posts,
     } );
};

const addPostsFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        loading: false
    });
};

const addPostReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.ADD_POST_START : return addPostsStart(state, action);
        case actionTypes.ADD_POST_SUCCESS : return addPostsSuccess(state, action);
        case actionTypes.ADD_POST_FAIL : return addPostsFail(state, action);
        default:
            return state;
    }
};

export default addPostReducer;