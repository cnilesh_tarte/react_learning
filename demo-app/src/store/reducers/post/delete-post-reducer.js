import * as actionTypes from '../../actions/actionTypes';
import { updateObject } from '../../../shared/utility';


const initialState = {
    posts : []
};

const deletePostsStart = ( state, action ) => {
    return updateObject( state, { error: null, loading: true } );
};

const deletePostsSuccess = (state, action) => {
    return updateObject( state, { 
        posts: action.posts,
     } );
};

const deletePostsFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        loading: false
    });
};


const deletePostReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.DELETE_POST_START : return deletePostsStart(state, action);
        case actionTypes.DELETE_POST_SUCCESS : return deletePostsSuccess(state, action);
        case actionTypes.DELETE_POST_FAIL : return deletePostsFail(state, action);
        default:
            return state;
    }
};

export default deletePostReducer;