import * as actionType from './../actionTypes';
import axios from 'axios';
// import { useAlert } from "react-alert";

export const deletePostStart = () => {
    return {
        type: actionType.DELETE_POST_START
    }
}

export const deletePostSuccess = (posts) => {
    return {
        type: actionType.DELETE_POST_SUCCESS,
        posts: posts,
    }
}

export const deletePostFail = (error) => {
    return {
        type: actionType.DELETE_POST_FAIL,
        error: error
    }
}


export const deletePost = (id) => async dispatch => {
    dispatch(deletePostStart());
    await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
        .then(res => {
            dispatch(deletePostSuccess(res.data));
        })
        .catch(err => {
            dispatch(deletePostFail(err))
        })
}