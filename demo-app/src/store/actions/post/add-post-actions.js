import * as actionType from './../actionTypes';
import axios from 'axios';
// import { useAlert } from "react-alert";

export const addPostStart = () => {
    return {
        type: actionType.ADD_POST_START
    }
}

export const addPostSuccess = (posts) => {
    return {
        type: actionType.ADD_POST_SUCCESS,
        posts: posts,
    }
}

export const addPostFail = (error) => {
    return {
        type: actionType.ADD_POST_FAIL,
        error: error
    }
}


export const addPost = (details) => async dispatch => {
    dispatch(addPostStart());
    await axios.post(`https://jsonplaceholder.typicode.com/posts/`, details )
        .then(res => {
            dispatch(addPostSuccess(res.data));
        })
        .catch(err => {
            dispatch(addPostFail(err));
        })
}