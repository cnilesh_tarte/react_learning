import * as actionType from './../actionTypes';
import axios from 'axios';
// import { useAlert } from "react-alert";

export const listPostStart = () => {
    return {
        type: actionType.LIST_POST_START
    }
}

export const listPostSuccess = (posts) => {
    return {
        type: actionType.LIST_POST_SUCCESS,
        posts: posts
    }
}

export const listPostFail = (error) => {
    return {
        type: actionType.LIST_POST_FAIL,
        error: error
    }
}


export const listPosts = () => async dispatch => {
    dispatch( listPostStart() )
    await axios.get('https://jsonplaceholder.typicode.com/posts')
        .then(res => {
            // dispatch({
            //     type: actionType.LIST_POST_SUCCESS,
            //     payload: res.data
            // })
            dispatch(listPostSuccess(res.data));
        })
        .catch(err => {
            // dispatch({
            //     type: actionType.LIST_POST_FAIL,
            //     payload: err
            // })
            dispatch(listPostFail(err))
        });
};
