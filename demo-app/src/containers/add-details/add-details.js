import React, { Component } from 'react';
import { checkValidity } from '../../shared/utility';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as actions from '../../store/actions/index';

export class AddDetailsComponent extends Component {

    state = {
        controls: {
            title: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Title'
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
            userType: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        { value: 'admin', displayValue: 'Admin' },
                        { value: 'user', displayValue: 'User' }
                    ]
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            }
        },
        isSignup: true
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true,
            }
        }
        this.setState({ controls: updatedControls });
    }

    submitHandler = (event) => {
        event.preventDefault();

        let details = { title : this.state.controls.title.value, userType : this.state.controls.userType.value}

        this.props.onAuth(details);
        if (this.props.data == null) {
            this.setState(prevState => ({
                ...prevState.controls,
                'email': {
                    'value': null
                }
            }));
        }
    }

    onChangeHandler = (event) => {

        let name = event.target.name;
        let value = event.target.value;

        this.setState(prevState => ({
            ...prevState.controls,
            [name]: {
                'value': value
            }
        }));
    }


    render() {

        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));


        return (
            <div className="login-container" >
                <div className="row">
                    <div className="col-sm-12 col-md=4 col-lg-4 mx-auto">
                        <div className="card">
                            <div className="card-header">
                                <strong>Add Details</strong>
                            </div>
                            <div className="card-body">
                                <form onSubmit={this.submitHandler}>
                                    {form}
                                    <Button btnType="btn btn-info" >SUBMIT</Button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { data: state.authData.error }
}

const mapDispatchToProps = dispatch => {
    return {
    onAuth: ( details ) => dispatch( actions.addPost( details ) ),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddDetailsComponent));