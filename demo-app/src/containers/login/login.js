import React, { Component } from 'react';
import SimpleReactValidator from 'simple-react-validator';
import './login.css';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import { checkValidity } from '../../shared/utility';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom';

export class LoginComponent extends Component {

    constructor() {
        super();
        this.validator = new SimpleReactValidator({ autoForceUpdate: this });
    }

    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false, 
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            },
            userType: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        { value: 'admin', displayValue: 'Admin' },
                        { value: 'user', displayValue: 'User' }
                    ]
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            }
        },
        isSignup: true
    }

    inputChangedHandler = ( event, controlName ) => { 
        const updatedControls = {
            ...this.state.controls,
            [controlName] : {
                ...this.state.controls[controlName],
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched : true,
            }   
        }
        this.setState( { controls: updatedControls } );
    }

    submitHandler = ( event ) => {
        event.preventDefault();
        this.props.onAuth( this.state.controls.email.value, this.state.controls.password.value );
        if(this.props.data == null){
            this.setState(prevState => ({
                ...prevState.controls,
                'email': {
                    'value': null
                }
            }));
        }
    }

    switchAuthModeHandler = () => {
        this.setState( prevState => {
            return { isSignup: !prevState.isSignup };
        });
    }


    onChangeHandler = (event) => {

        let name = event.target.name;
        let value = event.target.value;

        this.setState(prevState => ({
            ...prevState.controls,
            [name]: {
                'value': value
            }
        }));
    }

    render() {
        const formElementsArray = [];
        for ( let key in this.state.controls ) {
            formElementsArray.push( {
                id: key,
                config: this.state.controls[key]
            } );
        }

        let form = formElementsArray.map( formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={( event ) => this.inputChangedHandler( event, formElement.id )} />
        ) );


        return (
            <div className="login-container" >
                <div className="row">
                    <div className="col-sm-12 col-md=4 col-lg-4 mx-auto">
                        <div className="card">
                            <div className="card-header">
                                <strong>Login</strong>
                            </div>
                            <div className="card-body">
                            <form onSubmit={this.submitHandler}>
                                    {form}
                                    <Button btnType="btn btn-info" >SUBMIT</Button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {data: state.authData.error}
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: ( email, password ) => dispatch( actions.auth( email, password ) ),
    };
};

export default connect( mapStateToProps, mapDispatchToProps )(withRouter(LoginComponent));