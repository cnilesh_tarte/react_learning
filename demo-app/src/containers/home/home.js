import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import Aux from './../../hoc/Aux';

export class HomeComponent extends Component {

    state = {
        posts: [],
        title: null
    }

    componentDidMount = () => {
        this.props.listPost();
    }

    renderTable = () => {
        let listPost = this.props.data.posts
        if (listPost) {
            return (listPost.map(el => {
                return (
                    <tr key={el.id}>
                        <td>{el.title}</td>
                        <td><button onClick={() => { this.props.deletePost(el.id) }}> Delete</button></td>
                        <td><button > Edit</button></td>
                    </tr>
                )
            })
            )
        } else {
            return null
        }
    }

    render() {
        return (
            <div>
                <Aux>
                    <div className="container">
                        {/* <button type="button" className="btn btn-primary m-5" data-toggle="modal" data-target="#myModal">
                            Add Post
                        </button> */}
                        <table className="table table-hover">
                            <thead >
                                <tr>
                                    <th>Firstname</th>
                                    <th>Lastname</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            {this.renderTable()}
                        </table>
                    </div>
                    {/* <p>ldfygdygbfds</p> */}
                </Aux>
            </div>
        )
    }

}

const mapDispatchToProps = dispatch => {
    return {
        deletePost: (id) => dispatch(actions.deletePost(id)),
        listPost: () => dispatch(actions.listPosts()),
    };
};

const mapStateToProps = state => {
    return { data: state.listPosts }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);