import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export class HeaderComponent extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-sm bg-primary navbar-dark">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <Link className="nav-link" to="/login">Login</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/home">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/addDetails">Add Details</Link>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default HeaderComponent;