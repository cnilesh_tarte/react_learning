import React from 'react';

// import classes from './Button.css';

const button = (props) => {
    return (
        <button
        disabled={props.disabled}
        // className={[Button, classes[props.btnType]].join(' '), props.btnType}
        className={"Button m-2 "+ props.btnType}
        onClick={props.clicked}>{props.children}</button>
    );
}
export default button;